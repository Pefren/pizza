<?php namespace App\Listeners;

use App\Events\OrderSubmitted;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class LogOrderToFile
{
    /**
     * Handle the event.
     *
     * @param \App\Events\OrderSubmitted $order
     */
    public function handle(OrderSubmitted $order)
    {
	    $date = Carbon::now('Europe/Kiev');
	    $data = $order->data;
	    $phone = $data['phone'];
	    $name = $data['name'];
	    $street = $data['street'];
	    $house = $data['house'];
	    $apartment = $data['apartment'];
	    $entrance = $data['entrance'];
	    $floor = $data['floor'];
	    $change = $data['change'];
	    $comment = $data['comment'];
	    $call_me = $data['call_me'];

	    $cart = Session::get('cart');
	    $products_ids = array();
	    foreach ($cart as $item)
	    {
	    	$products_ids[] = $item['product_id'];
	    }
	    $products = Product::whereIn('id', $products_ids)->get();
	    foreach ($products as $index=>$product)
	    {
	    	foreach ($cart as $item)
		    {
		    	if($item['product_id'] == $product->id)
			    {
			    	if(isset($item['sauce_id']))
				    {
					    $products[$index]->sauce_id = $item['sauce_id'];
				    }
				    $products[$index]->count = $item['count'];
			    }
		    }
	    }

	    $devider = '****************************************************';
	    $next_line = PHP_EOL ;
	    $tab = "\t" ;
	    $log = '';
	    $log .= $devider . $next_line;
		$log .= 'Дата поступления заказа: ' . $date . $next_line;
		$log .= 'Номер телефона: ' . $phone . $next_line;
		$log .= 'Имя пользователя: ' . $name . $next_line;
		if($street!='')
		$log .= 'Улица доставки: ' . $street . $next_line;
	    if($house!='')
		$log .= 'Дом: ' . $house . $next_line;
	    if($apartment!='')
		$log .= 'Квартира: ' . $apartment . $next_line;
	    if($entrance!='')
		$log .= 'Подъезд: ' . $entrance . $next_line;
	    if($floor!='')
		$log .= 'Этаж: ' . $floor . $next_line;
	    if($change!='')
		$log .= 'Сдача: у меня ровно: ' . $change . $next_line;
	    if($comment!='')
		$log .= 'Комментарий: ' . $comment . $next_line;
		if($call_me == 1) $log .= 'Просьба позвонить' . $next_line;
		$log .= 'Приобретенные товары: ' . $next_line;
		foreach ($products as $product)
		{
			$log .= $tab . $product->generic_name . ' в количестве ' . $product->count . ' шт.' . ' по цене ' . $product->price . 'грн.';
			if($product->sauce_id !== null)
			{
				$sauce = Product::find($product->sauce_id);
				$log .= ' с соусом: ' . $sauce->title . $next_line;
			}
			else if($product->sauce_id === null && $product->category == 'pizza')
			{
				$sauce = Product::find(Product::DEFAULT_SAUCE_ID);
				$log .= ' с соусом: ' . $sauce->title . $next_line;
			}
			else
			{
				$log .= $next_line;
			}
		}
		$log .= 'Сумма заказа: ' . count_cart_sum() . 'грн.' . $next_line;
		$log .= $devider . $next_line . $next_line;

		$filename = "orders.txt";
	    $fileContents = file_get_contents($filename);
	    file_put_contents($filename, $log . $fileContents);
    }
}

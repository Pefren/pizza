<?php namespace App\Listeners;

use App\Events\OrderSubmitted;
use App\Product;
use Carbon\Carbon;
use App\Events\OrderSent;
use Illuminate\Support\Facades\Session;
use App\Events\OrderNotSent;

class SendOrderToPoster
{
    /**
     * Handle the event.
     *
     * @param \App\Events\OrderSubmitted $order
     */
    public function handle(OrderSubmitted $order)
    {
	    $date = Carbon::now('Europe/Kiev');
	    $data = $order->data;
	    $phone = $data['phone'];
	    $name = $data['name'];
	    $street = $data['street'];
	    $house = $data['house'];
	    $apartment = $data['apartment'];
	    $entrance = $data['entrance'];
	    $floor = $data['floor'];
	    $change = $data['change'];
	    if($change != '')
		    $change = '. Сдача: у меня ровно: ' . $change;
	    $comment = $data['comment'];
	    $call_me = $data['call_me'];
	    if($call_me == 1)
	    {
	    	$call_me_text = '. Позвоните мне.';
	    }
	    else
	    {
		    $call_me_text = '. Не звоните мне.';
	    }

	    $cart = Session::get('cart');
	    $ordered_sauces = [];

	    foreach ($cart as $index=>$item)
	    {
		    if(Product::find($item['product_id']) === null)
		    {
			    abort(400);
		    }
		    if(isset($item['modificator_id']))
		    {
			    $ordered_sauces[] = Product::find($item['modificator_id'])->title;
			    unset($cart[$index]['modificator_id']);
		    }
		    else
		    {
		    	if(Product::find($item['product_id'])->category == 'pizza')
			    {
				    $ordered_sauces[] = Product::find(Product::DEFAULT_SAUCE_ID)->title;
			    }
		    }
	    }

	    if($ordered_sauces != [])
	    {
		    $ordered_sauces = implode(', ', $ordered_sauces);
	    }
	    else
	    {
		    $ordered_sauces = '';
	    }

	    $url = 'https://mokopizzadev.joinposter.com/api/incomingOrders.createIncomingOrder?token=0597127b524e16e119bfa12040f63d87';

	    //dd($cart);

	    $incoming_order = [
		    'spot_id'   => 1,
		    'phone'     => $phone,    //рандомизация номера телефона
		    'address'   => 'ул. ' . $street . ', дом ' . $house .', кв. ' . $apartment . ' , под. ' . $entrance . ' , эт ' . $floor,
		    'first_name' => $name,
		    'comment' => $comment . $change . $call_me_text . ' ' . $ordered_sauces,
		    'products'  => $cart,
	    ];

	    $response = $this->sendRequest($url, 'post', $incoming_order);
		$encode_response = json_decode ($response);
	    if(isset($encode_response->response->status) && $encode_response->response->status == 0)
	    {
	    	event(new OrderSent($order->data));
	    }
	    else
	    {
		    event(new OrderNotSent($order->data));
	    }
    }

	function sendRequest($url, $type = 'get', $params = [], $json = false)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		if ($type == 'post' || $type == 'put') {
			curl_setopt($ch, CURLOPT_POST, true);

			if ($json) {
				$params = json_encode($params);

				curl_setopt($ch, CURLOPT_HTTPHEADER, [
					'Content-Type: application/json',
					'Content-Length: ' . strlen($params)
				]);

				curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			} else {
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
			}
		}

		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Poster (http://joinposter.com)');

		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}
}

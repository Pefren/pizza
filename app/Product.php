<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

	/**
	 * Default sauce id.
	 */
	const DEFAULT_SAUCE_ID = 170;

	/**
	 * Check if current product is pizza
	 *
	 * @return boolean
	 * */
	public function is_pizza()
	{
		if($this->category == 'pizza')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
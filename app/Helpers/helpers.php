<?php

/**
 * Count sum of products that added to cart
 *
 * @return float $sum
 * */
function count_cart_sum()
{
	$sum = 0;
	if(Illuminate\Support\Facades\Session::has('cart'))
	{
		$cart = Illuminate\Support\Facades\Session::get('cart', []);
		$positions_ids = array();
		foreach ($cart as $item)
		{
			$positions_ids[] = $item['product_id'];
		}
		$positions = App\Product::whereIn('id', $positions_ids)->get();
		foreach ($positions as $position)
		{
			foreach ($cart as $item)
			{
				if($position->id == $item['product_id'])
				{
					$sum += $item['count'] * $position->price;
				}
			}
		}
	}

	return $sum;
}

/**
 * Return boolean that means that product can be with crust
 *
 * @return boolean $has_crust
 * */
function get_crust_available_by_id($id)
{
	$has_crust = false;
	$product = App\Product::find($id);
	$products = App\Product::where('slug', $product->slug)->get();
	foreach ($products as $product_item)
	{
		if($product_item->crust)
			$has_crust=true;
	}

	return $has_crust;
}

/**
 * Return sizes of product
 *
 * @return array $sized
 * */
function get_sizes_available_by_id($id)
{
	$sizes = array();
	$product = App\Product::find($id);
	$products = App\Product::where('slug', $product->slug)->get();
	foreach ($products as $product_item)
	{
		$sizes[] = $product_item->size;
	}

	return array_unique($sizes);
}

/**
 * Return count of cart products
 *
 * @return integer $count
 */
function count_of_cart()
{
	$count = 0;
	if(Illuminate\Support\Facades\Session::has('cart'))
	{
		$cart = Illuminate\Support\Facades\Session::get('cart', []);
		foreach ($cart as $item)
		{
			$count += $item['count'];
		}
	}

	return $count;
}

<?php

namespace App\Http\ViewComposers;

use App\Http\Controllers\CartController;
use Illuminate\View\View;
use Illuminate\Support\Facades\Session;
use App\Product;

class SaucesComposer
{

	/**
	 * Create a new profile composer.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$sauces = Product::where('category', 'sauce')->get();

		$view->with([
			'sauces' => $sauces
		]);
	}
}
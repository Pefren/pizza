<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class CartController extends Controller
{

	/**
	 * Render cart page with positions from cart
	 *
	 * @return View
	 * */
    public function index()
    {
	    if(Session::has('cart'))
	    {
		    $cart = Session::get('cart');
		    $positions_ids = array();
		    foreach ($cart as $item)
		    {
			    $positions_ids[] = $item['product_id'];
		    }
		    $positions = Product::whereIn('id', $positions_ids)->get();
		    foreach ($positions as $position)
		    {
			    foreach ($cart as $item)
			    {
				    if($position->id == $item['product_id'])
				    {
					    $position->count = $item['count'];
					    if(isset($item['modificator_id']))
					    {
						    $position->modificator_id = $item['modificator_id'];
					    }
				    }
			    }
		    }
	    }
	    else
	    {
	    	$positions = array();
	    }

    	return view('cart.index')->with(compact('positions' ));
    }

    /**
     * Ajax adding product in cart
     *
     * @return array
     * */
    public function add(Request $request)
    {
    	$data = $request->all();

    	//Session::flush();

	    if(Product::find($data['product_id']) === null)
	    {
	    	abort(400);
	    }

	    $add_to_cart = [
		    'product_id' => intval($data['product_id']),
		    'count' => 1
	    ];

	    if(Session::has('cart'))
	    {
	    	$cart = Session::get('cart');
		    $cart = array_values($cart);
			$found = false;
	        foreach ($cart as $index=>$item)
		    {
		        if($item['product_id'] == $data['product_id'])
			    {
				    $cart[$index]['count']++;
				    Session::put('cart', $cart);
				    $found = true;
			    }
		    }
		    if(!$found)
		    {
			    Session::push('cart', $add_to_cart);
		    }
	    }else{
		    Session::push('cart', $add_to_cart);
	    }

		$cart = Session::get('cart');
	    $sum = count_cart_sum();

	    return ['sum' => $sum, 'count' => count_of_cart()];
    }

    /**
     * Ajax changing quantity of products in cart
     *
     * @return array
     * */
	public function change_quantity(Request $request)
	{
		$data = $request->all();

		//Session::flush();

		$add_to_cart = [
			'product_id' => $data['product_id'],
			'count' => $data['count']
		];

		if(Session::has('cart'))
		{
			$cart = Session::get('cart');
			$cart = array_values($cart);
			$found = false;
			foreach ($cart as $index=>$item)
			{
				if($item['product_id'] == $data['product_id'])
				{
					$cart[$index]['count'] = intval($data['count']);
					Session::put('cart', $cart);
					$found = true;
				}
			}
			if(!$found)
			{
				Session::push('cart', $add_to_cart);
			}
		}else{
			Session::push('cart', $add_to_cart);
		}
		$cart = Session::get('cart');
		$sum = count_cart_sum();

		return ['sum' => $sum, 'count' => count_of_cart()];
	}

	/**
	 * Ajax removing product from cart
	 *
	 * @return array
	 * */
	public function remove(Request $request)
	{
		$data = $request->all();
		$product_id = $data['product_id'];
		$cart = Session::get('cart');
		foreach ($cart as $index=>$item)
		{
			if($item['product_id'] == $product_id)
			{
				unset($cart[$index]);
			}
		}
		Session::put('cart',$cart);
		$sum = count_cart_sum();

		return ['sum' => $sum, 'count' => count_of_cart()];
	}
}

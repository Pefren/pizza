<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Events\OrderSubmitted;

class OrderController extends Controller
{
    /**
     * Get order data and send to API
     *
     * @param Request $request
     * @return mixed
     * */
    public function checkout(Request $request)
    {
	    $data = $request->only([
	    	'phone',
		    'name',
		    'street',
		    'house',
		    'apartment',
		    'entrance',
		    'floor',
		    'change',
		    'comment',
		    'call_me'
	    ]);

	    $rules = [
		    'phone' => ['required'],
		    'name' => ['required'],
		    'comment' => ['max:100']
	    ];

	    $validation = Validator::make($data, $rules);

	    if ($validation->fails())
	    {
		    return redirect()
			    ->back()
			    ->withErrors($validation);
	    }

	    if(count_cart_sum() < 200)
	    {
		    return redirect()
			    ->back()
			    ->withErrors(['Сумма заказа должна быть больше 200грн.']);
	    }

	    if(!isset($data['call_me']))
	    	$data['call_me'] = 0;

	    event(new OrderSubmitted($data));

	    Session::forget('cart');

	    return back()
		    ->withMessage('Вы успешно оформили заказ!');
    }
}

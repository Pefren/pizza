<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	/**
	 * Filter pizzas by its slug, size, crust
	 *
	 * @param Request $request
	 * @return array
	 * */
	public function filter(Request $request)
	{
		$data = $request->all();
		$slug = $data['slug'];
		$size = $data['size'];
		$crust = $data['crust'];

		$product = Product::where('slug',   $slug)
		                  ->where('size',   $size)
		                  ->where('crust',  $crust)
		                  ->first();

		if($product != '')
		{
			return [
				'product_id'    => $product->id,
				'price'         => $product->price
			];
		}
	}

	/**
	 * Change sauce id in cart
	 *
	 * @param Request $request
	 * @return void
	 * */
	public function change_sauce(Request $request)
	{
		$data = $request->all();
		$product_id = $data['product_id'];
		$sauce_id = $data['sauce_id'];

		$cart = Session::get('cart');
		foreach ($cart as $index=>$item)
		{
			if($item['product_id'] == $product_id)
			{
				$cart[$index]['modificator_id'] = intval($sauce_id);
			}
		}
		Session::put('cart', $cart);
	}
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Product;

class CsvController extends Controller
{

	/**
	 * Create a new CsvController instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('admin');
	}

	/**
	 * Return csv index page with import csv form.
	 *
	 * @return View
	 */
	public function index()
	{
		return view('csv.index');
	}

	/**
	 * Import csv file to database with products data
	 *
	 * @param Illuminate\Http\Request
	 * @return string
	 * */
	public function import( Request $request )
	{
		header( 'Content-Type: text/html; charset=UTF-8' );
		if ( isset( $_POST["Import"] ) )
		{
			$filename = $_FILES["file"]["tmp_name"];
			if ( $_FILES["file"]["size"] > 0 )
			{
				$otput              = array();
				$file               = fopen( $filename, "r" );

				while ( $getData    = fgetcsv( $file, 100000, ";" ) )
				{
					foreach ( $getData as $index => $data )
					{
						$getData[ $index ] = mb_convert_encoding( $data, "UTF-8", "Windows-1251" );
					}

					if(is_numeric($getData[0]))
					{
						$product = Product::find( $getData[0] );
						if ( $product == '' )
						{
							$product = new Product();
							$product->id  = $getData[0];
							$product->generic_name  = $getData[1];
							$product->title         = $getData[2];
							$product->crust         = $getData[3];
							$product->size          = $getData[4];
							$product->category      = $getData[5];
							$product->slug          = $getData[6];
							$product->price         = $getData[7];
							$product->weight        = $getData[8];
							$product->description   = $getData[9];
							$product->save();
						}
						else {
							$product->generic_name  = $getData[1];
							$product->title         = $getData[2];
							$product->crust         = $getData[3];
							$product->size          = $getData[4];
							$product->category      = $getData[5];
							$product->slug          = $getData[6];
							$product->price         = $getData[7];
							$product->weight        = $getData[8];
							$product->description   = $getData[9];
							$product->save();
						}
					}
				}
				fclose( $file );

				return back();
			}
		}
	}
}
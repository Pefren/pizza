<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SiteController extends Controller
{
	/**
	 * Render main page with products
	 *
	 * @return View
	 * */
    public function index(Request $request)
    {
    	$pizzas = Product::where('category', 'pizza')
	                     ->groupBy('slug')
			     ->orderBy('id')
	                     ->get();

    	$snacks = Product::where('category', 'snack')
	                     ->get();

    	$drinks = Product::where('category', 'drink')
	                     ->get();

    	$deserts = Product::where('category', 'desert')
	                      ->get();

	    return view('home.index')->with(compact('pizzas', 'snacks', 'drinks', 'deserts'));
    }
}
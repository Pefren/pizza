<?php namespace App\Events;

class OrderSent extends Event
{
    /**
     * Order instance.
     *
     * @var array $data
     */
    public $data;

    /**
     * Create a new event instance.
     *
     * @param array $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
}
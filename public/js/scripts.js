jQuery(document).ready(function(){

    $('input[type="radio"]').styler();

    $('.main_slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: true,
        dots: true,
      infinite: true
    });

	$('#toggler').on('click',function(){
		$('#left-sidebar').toggleClass('open');
		$('#toggler').toggleClass('open');
		$('.header_mob').toggleClass('open');
	})
	$('#left-sidebar .close').on('click',function(){
		$('#left-sidebar').removeClass('open');
		$('#toggler').removeClass('open');
		$('.header_mob').toggleClass('open');
	})

        function navigation(){
          if($(window).scrollTop() > 160) {
            $('.header_w').addClass('fixed');
        } else {
            $('.header_w').removeClass('fixed');
        }
    }
    navigation();
    $(window).scroll(function(){
      navigation();
      
    })

    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 50
        }, 500);
    });
});

$('#show_checkout_form').on('click', function(){
    var is_clicked = parseInt($(this).attr('is_clicked'));
    if(is_clicked == 0)
    {
        $(this).attr('is_clicked', 1);
        $('.cart_w_w').hide();
        $('.form_cart_w').show();
        $('html, body').animate({scrollTop: 0},500);
    }
    else
    {
        var telInput = $("#phone"),
            errorMsg = $("#error-msg");
        if(telInput.val() == '' || telInput.intlTelInput("isValidNumber")) $('#checkout-form').submit();
        else {
            telInput.addClass("error");
            errorMsg.removeClass("hide");
            return false;
        }
    }
});

$('.add_to_cart').on('click', function(event){
    event.preventDefault();

    var html =
        '<div class="h9"><p>в заказе</p></div>' +
        '<div class="plus_minus_w">' +
            '<div class="flex flex_m">' +
            '<div class="minus"><button></button></div>' +
            '<div class="number"><input type="text" value="1" disabled="" class="qty_input"></div>' +
            '<div class="plus active"><button></button></div>' +
            '</div>' +
        '</div>';

    $(this).closest('.add_to_cart_block').find('.product_f').append(html);
    $(this).closest('.add_to_cart_block').find('.h8').hide().fadeIn(500);
    $(this).closest('.add_to_cart_block').find('.h9').hide().fadeIn(500);
    $(this).closest('.add_to_cart_block').find('.plus_minus_w').hide().fadeIn(500);
    $(this).closest('.add_to_cart_block').addClass('active');

    // add to cart
    var product_id = parseInt($(this).attr('product-id'));
    var quantity = parseInt($(this).closest('.add_to_cart_block').find('.qty_input').val());
    add_to_cart(product_id, quantity);

    $( ".plus_minus_w .minus" )
        .mouseenter(function() {
            $(this).addClass('active');
        })
        .mouseleave(function() {
            $(this).removeClass('active');
        });

    $( ".plus_minus_w .plus" ).on('click', function () {
        var qty = parseInt($(this).closest('.plus_minus_w').find('.qty_input').val());
        $(this).closest('.plus_minus_w').find('.qty_input').val(qty+1);
        var product_id = parseInt($(this).closest('.add_to_cart_block').find('.add_to_cart').attr('product-id'));
        // change quantity
        change_quantity(product_id, qty+1);

    });
    $( ".plus_minus_w .minus" ).on('click', function () {
        var qty = parseInt($(this).closest('.plus_minus_w').find('.qty_input').val());
        if(parseInt(qty)>1)
        {
            $(this).closest('.plus_minus_w').find('.qty_input').val(qty-1);
            var product_id = parseInt($(this).closest('.add_to_cart_block').find('.add_to_cart').attr('product-id'));
            //change quantity
            change_quantity(product_id, qty-1);
        }
    });
    
});

$('.cart-remove').on('click',function(event)
{
    event.preventDefault();
    var product_id = $(this).attr('product-id');
    var data = 'product_id=' + product_id;
    $.ajax(
        {
            url: '/cart/remove',
            type: "POST",
            dataType: 'text',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            success: function (data, textStatus, jqXHR) {
                if(data != null || data != '')
                {
                    var output = JSON.parse(data);
                    remove_cart_waste_items(product_id, output['sum'], output['count']);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        }
    );
});

function add_to_cart(product_id,quantity)
{
    var data = 'product_id=' + product_id + '&count=' + quantity;

    $.ajax(
        {
            url: '/cart/add',
            type: "POST",
            dataType: 'text',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            success: function (data, textStatus, jqXHR) {
                var output = JSON.parse(data);
                change_cart(product_id, output['sum'], output['count']);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        }
    );
}

function change_quantity(product_id,quantity)
{
    var data = 'product_id=' + product_id + '&count=' + quantity;

    $.ajax(
        {
            url: '/cart/change_quantity',
            type: "POST",
            dataType: 'text',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            success: function (data, textStatus, jqXHR) {
                var output = JSON.parse(data);
                change_cart(product_id, output['sum'], output['count']);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        }
    );
}

function remove_cart_waste_items(product_id, sum, count)
{
    $('#cart-product-' + product_id).html('<h3 class="empty-cart-position h13">Товар удален</h3>');
    $('.final_price strong').html(sum);
    $('.cart-sum').html(sum + ' грн <span>' + count + '</span>');
}

function change_cart(product_id, sum, count)
{
    $('.final_price strong').html(sum);
    $('.cart-sum').html('<strong>' + sum + ' грн</strong> <span>' + count + '</span>');
}

/**
* Product attributes changing
* */

$('.change_size').on('click', function(event){
    event.preventDefault();
    var button = $(this);
    var product_item = $(this).closest('.product');
    button.closest('.product_size').find('.active').removeClass('active');
    button.addClass('active');

    var slug = product_item.find('.slug').val();
    var size = parseInt(button.find('span').html());
    product_item.find('.size').val(size);
    if(size == 25)
    {
        crust = 1;
        product_item.find('.no_crust').hide();
        product_item.find('.with_crust').show();
        product_item.find('.no_crust .jq-radio').removeClass('checked');
        product_item.find('.with_crust .jq-radio').addClass('checked');
        product_item.find('.with_crust input[type=radio]').attr('checked','');
        product_item.find('.has_crust').val(1);
    }
    else if(size == 35)
    {
        crust = 0;
        product_item.find('.no_crust').show();
        product_item.find('.with_crust .jq-radio').removeClass('checked');
        product_item.find('.no_crust .jq-radio').addClass('checked');
        product_item.find('.with_crust').hide();
        product_item.find('.no_crust input[type=radio]').attr('checked','');
        product_item.find('.has_crust').val(0);
    }
    else
    {
        // when price is 30 uah
        var crust = parseInt(button.closest('.product').find('.has_crust').val());

        if(slug === 'pizza-pirog')
        {
            product_item.find('.no_crust').hide();
            product_item.find('.no_crust .jq-radio').removeClass('checked');
            product_item.find('.with_crust .jq-radio').addClass('checked');
        }
        else
        {
            product_item.find('.no_crust').show();
            product_item.find('.with_crust').show();
            product_item.find('.with_crust .jq-radio').addClass('checked');
        }
        product_item.find('.has_crust').val(1);
        product_item.find('.no_crust .jq-radio').removeClass('checked');
    }

    get_product_by(slug,size,crust);
});

$('.change_crust').on('click', function(event){
    var checkbox = $(this);
    var product_item = $(this).closest('.product');
    var crust =parseInt(checkbox.val());
    product_item.find('.has_crust').val(crust);
    var slug = product_item.find('.slug').val();
    var size = parseInt(product_item.find('.size').val());

    get_product_by(slug,size,crust);
});

function get_product_by(slug, size, crust)
{
    var data = 'slug=' + slug + '&size=' + size + '&crust=' + crust;

    $.ajax(
        {
            url: '/product/filter',
            type: "POST",
            dataType: 'text',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            success: function (data, textStatus, jqXHR) {
                var output = JSON.parse(data);
                update_product_data(slug, output['product_id'], output['price']);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        }
    );
}

function update_product_data(slug, product_id, price)
{
    var product_item= $('.slug-' + slug);
    product_item.find('.product_id').val(product_id);
    product_item.find('.add_to_cart').attr('product-id', product_id);
    product_item.find('.price').html('<p>' + price +'<span>грн</span></p>');
}


/**
 * Change quantity in cart
 * */

$( ".plus_minus_w .minus" )
    .mouseenter(function() {
        $(this).addClass('active');
    })
    .mouseleave(function() {
        $(this).removeClass('active');
    });

$( ".plus_minus_w .minus" ).on('click', function () {
    var qty = parseInt($(this).closest('.plus_minus_w').find('.qty_input').val());
    if(parseInt(qty)>1)
    {
        $(this).closest('.plus_minus_w').find('.qty_input').attr('value', qty-1);
        var product_id = parseInt($(this).closest('.cart').find('.product_id').val());
        //change quantity
        change_quantity(product_id, qty-1);
    }
});

$( ".plus_minus_w .plus" ).on('click', function () {
    var qty = parseInt($(this).closest('.plus_minus_w').find('.qty_input').val());
    $(this).closest('.plus_minus_w').find('.qty_input').attr('value', qty+1);
    var product_id = parseInt($(this).closest('.cart').find('.product_id').val());
    // change quantity
    change_quantity(product_id, qty+1);
});


$('.change_sauce').on('change', function(){
    var sauce_id = $(this).val()
    var product_id = $(this).closest('.cart').find('.product_id').val();
    var data = 'sauce_id='+sauce_id+'&product_id='+product_id;

    $.ajax(
        {
            url: '/product/change_sauce',
            type: "POST",
            dataType: 'text',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            success: function (data, textStatus, jqXHR) {
                console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        }
    );
});
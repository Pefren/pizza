@if (count($errors) > 0)
    <div style="padding: 15px;">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li style="list-style-type: none;">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
@if (Session::get('message')!='')
    <div style="padding: 15px;">
        <div class="alert alert-success">
            <ul>
                <li style="list-style-type: none;">{{ Session::get('message') }}</li>
            </ul>
        </div>
    </div>
@endif

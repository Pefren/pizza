@extends('layouts.main')

@section('content')
    <div id="container1">
        <div class="main_slider">
           <div class="container1_img"><img src="img/bg2.jpg" alt=""></div>
           <div class="container1_img"><img src="img/bg1.jpg" alt=""></div>  
        </div>
        
    </div>
    <div id="container2">
        <div class="inside">
            <div class="container2">
                <div class="product_w" id="pizzas">
                    <div class="flex flex_sb">
                        @foreach($pizzas as $product)
                            @include('home._pizza_item')
                        @endforeach
                    </div>
                </div>
                <div class="h10" id="drinks"><p>Напитки</p></div>
                <div class="product_w">
                    <div class="flex flex_sb">
                        @foreach($drinks as $product)
                            @include('home._product_item')
                        @endforeach
                    </div>
                </div>
                <div class="h10" id="snacks"><p>Закуски</p></div>
                <div class="product_w">
                    <div class="flex flex_sb">
                        @foreach($snacks as $product)
                            @include('home._product_item')
                        @endforeach
                    </div>
                </div>
                <div class="h10" id="deserts"><p>Десерты</p></div>
                <div class="product_w">
                    <div class="flex flex_sb">
                        @foreach($deserts as $product)
                            @include('home._product_item')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
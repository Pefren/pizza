<div class="product slug-{{ $product->slug }}">
    <input type="hidden" class="product_id" value="{{ $product->id }}">
    <input type="hidden" class="slug" value="{{ $product->slug }}">
    <input type="hidden" class="has_crust" value="1">
    <input type="hidden" class="size" value="">

    <div class="product_img"><img src="{{ asset('product_images/' . $product->slug . '_small.jpg') }}" alt=""></div>
    <div class="product_txt">
        <div class="product_txt_hw">
            <div class="h5"><p>{{ $product->title }}</p></div>
            <div class="h6"><p>{{ $product->description }}</p></div>
        </div>
        <div class="product_size">
            <ul class="flex">
                <?php $sizes = get_sizes_available_by_id($product->id); ?>
                @if(is_array($sizes))
                    @foreach($sizes as $index => $size)
                        <li class="change_size @if($index==0) active @endif"><span>{{ $size }}</span> см</li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="filter">
            <ul class="flex crust_choosing_block">
                @if(get_crust_available_by_id($product->id))
                    <li @if($product->size == 35) class="flex with_crust" style="display: none" @else class="flex with_crust active" @endif>
                        <div class="filter_input">
                            <input id="wert_{{ $product->id }}_on" class="change_crust" name="crust-{{ $product->id }}" type="radio" checked="" value="1">
                        </div>
                        <div class="filter_label">
                            <label for="wert_{{ $product->id }}_on">С бортиком</label>
                        </div>
                    </li>
                    <li class="flex no_crust" @if($product->size == 25) style="display: none" @endif>
                        <div class="filter_input">
                            <input id="wert_{{ $product->id }}_off" class="change_crust" name="crust-{{ $product->id }}" type="radio" value="0">
                        </div>
                        <div class="filter_label">
                            <label for="wert_{{ $product->id }}_off">Без бортика</label>
                        </div>
                    </li>
                @else
                    <li class="flex active">
                        <div class="filter_input">
                            <input id="wert_{{ $product->id }}_off" name="crust-{{ $product->id }}" type="radio" value="1" checked>
                        </div>
                        <div class="filter_label">
                            <label for="wert_{{ $product->id }}_off">Без бортика</label>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    <div class="product_f_w add_to_cart_block">
        <div class="flex flex_m flex_sb product_f">
            <div class="h7 price"><p>{{ $product->price }}<span>грн</span></p></div>
            <div class="h8"><a href="" class="add_to_cart" product-id="{{ $product->id }}">Заказать</a></div>
        </div>
    </div>

    {{-- <div class="product_f_w active">
         <div class="flex flex_m flex_sb product_f">
             <div class="h7"><p>250<span>грн</span></p></div>
             <div class="h8"><a href="">Заказать</a></div>
             <div class="h9"><p>в заказе</p></div>
             <div class="plus_minus_w">
                 <div class="flex flex_m">
                     <div class="minus"><button></button></div>
                     <div class="number"><input type="text" placeholder="1" disabled=""></div>
                     <div class="plus active"><button></button></div>
                 </div>
             </div>
         </div>
     </div>--}}
</div>
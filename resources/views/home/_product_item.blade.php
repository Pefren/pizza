<div class="product product2 slug-{{ $product->slug }}">
    <div class="product_img"><img src="{{ asset('product_images/' . $product->slug . '_small.jpg') }}" alt=""></div>
    <div class="product_txt">
        <div class="h5"><p>{{ $product->title }}</p></div>
    </div>
    <div class="product_f_w add_to_cart_block">
        <div class="flex flex_m flex_sb product_f">
            <div class="h7 price"><p>{{ $product->price }}<span>грн</span></p></div>
            <div class="h8"><a href="" class="add_to_cart" product-id="{{ $product->id }}">Заказать</a></div>
        </div>
    </div>
</div>
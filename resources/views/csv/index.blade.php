@extends('layouts.main')

@section('content')
    <form class="form-horizontal" action="{{ route('csv.import') }}" method="post" name="upload_excel" enctype="multipart/form-data">
        <fieldset>
            {{ csrf_field() }}
            <legend>Import your CSV</legend>

            <!-- File Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="filebutton">Select File</label>
                <div class="col-md-4">
                    <input type="file" name="file" id="file" class="input-large">
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton">Import data</label>
                <div class="col-md-4">
                    <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading...">Import</button>
                </div>
            </div>

        </fieldset>
    </form>
@endsection
@extends('layouts.main')

@section('content')

    <div id="container3">
        <div class="inside">
            <div class="container3">
                <div class="flex flex_sb">
                    <div class="cart_w_w">
                        <div class="h10">Ваша корзина <span>Доставка бесплатно. Районы: Центральный, Кальмиусский и Приморский.<br> Минимальный заказ - 200 грн.</span></div>
                        @include('errors._errors_messages')
                        <div class="cart_w">
                            @if(count($positions) > 0)
                                @foreach($positions as $position)
                                    <div class="cart" id="cart-product-{{ $position->id }}">
                                        <input type="hidden" class="product_id" value="{{ $position->id }}">
                                        <div class="flex flex_m">
                                            <div class="cart_img"><img src="{{ asset('product_images/' . $position->slug . '_small.jpg') }}" alt=""></div>
                                            <div class="cart_txt">
                                                <div class="h13">{{ $position->title }}</div>
                                                @if($position->is_pizza())
                                                    <div class="flex">
                                                        <div class="h14 h14_1"><p>@if($position->crust)С бортиком,@elseБез бортика,@endif {{ $position->size }} см</p></div>
                                                        <div class="h14 h14_2"><p>Бесплатный соус:</p></div>
                                                        <div class="select_w">
                                                            <select class="change_sauce">
                                                                @foreach($sauces as $index=>$sauce)
                                                                    <option
                                                                            @if(
                                                                                ($position->modificator_id === null && $sauce->id === App\Product::DEFAULT_SAUCE_ID)
                                                                                OR
                                                                                ($position->modificator_id !== '' && $sauce->id == $position->modificator_id)
                                                                            )
                                                                                selected="selected"
                                                                            @endif
                                                                            value="{{ $sauce->id }}">

                                                                        {{ $sauce->title }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="cart_price_w flex flex_m">
                                                <div class="plus_minus_w">
                                                    <div class="flex flex_m">
                                                        <div class="minus"><button></button></div>
                                                        <div class="number"><input type="text" value="{{ $position->count }}" disabled="" class="qty_input"></div>
                                                        <div class="plus active"><button></button></div>
                                                    </div>
                                                </div>
                                                <div class="cart_price">
                                                    <div class="h15">{{ $position->price }} <span>грн</span></div>
                                                </div>
                                            </div>
                                            <div class="cancel">
                                                <div><a><img src="{{ asset('img/cancel.png') }}" alt="" class="cart-remove cursor-pointer" product-id="{{ $position->id }}"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="cart" id="cart-product-91"><h3 class="empty-cart-position h13">Корзина пуста</h3></div>
                            @endif
                            <div class="cart_footer">
                                <div class="flex">
                                    <div class="form_w">
                                        <form>
                                            <input type="text" placeholder="Купон на скидку">
                                            <button>Применить</button>
                                        </form>
                                    </div>
                                    <div class="final_price">
                                        <div class="h16"><p>Итого:<strong>{{ $sum }}</strong><span>грн</span></p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="back"><a href="{{ route('site.index') }}">Назад в меню</a></div>
                        <div class="cart_footer mob">
                            <div class="flex">
                                <div class="form_w">
                                    <form>
                                        <input type="text" placeholder="Купон на скидку">
                                        <button>Применить</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form_cart_w">
                        <div class="h17"><p>Оформление заказа</p></div>
                        <div class="form_cart">
                            <form action="{{ route('order.checkout') }}" method="POST" id="checkout-form">
                                {{ csrf_field() }}
                                <div class="input1 flex flex_sb">
                                    <div class="input_star">
                                        <input type="text"
                                               id="phone"
                                               placeholder="Телефон"
                                               @if(old('name')=='') value="+380" @else value="{{ old('phone') }}" @endif
                                               required
                                               name="phone"
                                                maxlength="13">
                                        <div id="error-msg" class="hide">Пожалуйста, введите корректно номер телефона, начиная с +380</div>
                                    </div>

                                    <div class="input1_1 input_star"><input type="text" placeholder="Имя" required name="name" value="{{ old('name') }}"></div>
                                </div>
                                <div class="input1 flex flex_sb">
                                    <div><input type="text" placeholder="Улица (переулок, проспект)" name="street"></div>
                                    <div class="input1_1"><input type="text" placeholder="Дом" name="house"></div>
                                </div>
                                <div class="input1 input2 flex flex_sb">
                                    <div class="input1_1"><input type="text" placeholder="Квартира" name="apartment"></div>
                                    <div class="input1_1"><input type="text" placeholder="Подъезд" name="entrance"></div>
                                    <div class="input1_1"><input type="text" placeholder="Этаж" name="floor"></div>
                                </div>
                                <div class="filter filter_wert">
                                 <ul class="flex">
                                    <li class="flex">
                                       <div class="filter_input">
                                          <input id="wert_100" name="radio100" type="radio" checked="">
                                       </div>
                                       <div class="filter_label">
                                          <label for="wert_100">Оплата наличными</label>
                                       </div>
                                    </li>
                                    <li class="flex">
                                       <div class="filter_input">
                                          <input id="wert_101" name="radio100" type="radio">
                                       </div>
                                       <div class="filter_label">
                                          <label for="wert_101">Картой</label>
                                       </div>
                                    </li>
                                 </ul>
                                </div>
                                <div class="input1 input3">
                                    <div><input type="text" placeholder="С какой суммы подготовить сдачу?" name="change"></div>
                                </div>
                                <div class="input1 input3">
                                    <div><input type="text" placeholder="Комментарий (например, код домофона)" name="comment" maxlength="100"></div>
                                </div>
                                <div class="filter filter_cart">
                                    <ul class="flex">
                                        <li class="flex active">
                                            <div class="filter_input">
                                                <input id="wert_100" type="checkbox" name="call_me" value="1">
                                            </div>
                                            <div class="filter_label">
                                                <label for="wert_100">Обязательно позвоните мне</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="cart_button"><button>Отправить заказ</button></div>
                            </form>
                        </div>
                    </div>
                    <div class="cart_footer_mob">
                        <div class="flex flex_sb flex_m final_price">
                            <div class="h16"><p>Итого:<strong>{{ $sum }}</strong><span>грн</span></p></div>
                            <div class="order_go"><a class="cursor-pointer" id="show_checkout_form" is_clicked="0">Оформить заказ</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css_includes')
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
@endsection

@section('js_includes')
    <script src="{{ asset('js/intlTelInput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/utils.js') }}" type="text/javascript" ></script>


    <script>
        var telInput = $("#phone"),
            errorMsg = $("#error-msg");

        // initialise plugin
        telInput.intlTelInput({
            utilsScript: "{{ asset('js/utils.js') }}"
        });

        var reset = function() {
            telInput.removeClass("error");
            errorMsg.addClass("hide");
        };

        // on blur: validate
        telInput.blur(function() {
            reset();
            if ($.trim(telInput.val())) {
                if (telInput.intlTelInput("isValidNumber")) {
                } else {
                    telInput.addClass("error");
                    errorMsg.removeClass("hide");
                }
            }
        });

        // on keyup / change flag: reset
        telInput.on("keyup change", reset);

        $('.cart_button button').click(function(event) {
            if(telInput.val() == '' || telInput.intlTelInput("isValidNumber")) return true;
            else {
                telInput.addClass("error");
                errorMsg.removeClass("hide");
                return false;
            }
        });
    </script>
@endsection


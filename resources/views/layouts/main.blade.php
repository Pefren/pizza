<!DOCTYPE html>
<html>
<head>
    <title>Моко пицца</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="{{ asset('css/jquery.formstyler.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/media.css') }}" rel="stylesheet" type="text/css">
    @yield('css_includes')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="@if(URL::current() == route('site.index')) front @elseif(URL::current() == route('cart.index')) cart_page @endif">
<div class="wrapper">
    @include('layouts._header')
    <main>
        @yield('content')
    </main>
    @include('layouts._footer')
</div>
<div id="container100">
    <div class="inside">
        <div class="container100">
        </div>
    </div>
</div>
<script src="{{ asset('js/jquery-3.3.1.min.js') }}">  </script>
<script src="{{ asset('js/jquery.formstyler.min.js') }}">  </script>
<script src="{{ asset('js/slick.js') }}">  </script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{ asset('js/scripts.js') }}">  </script>
@yield('js_includes')
</body>
</html>


<footer>
    <div class="inside">
        <div class="footer">
            <div class="flex flex_sb">
                <div class="footer_logo flex">
                    <div class="footer_logo_img"><img src="{{ asset('img/logo1.png') }}" alt=""></div>
                    <div>
                        <div class="h11">© Моко Пицца, 2018</div>
                        <div class="h12"><a href="">Правовая информация</a></div>
                    </div>
                </div>
                <div class="footer_mail_tel">
                    <div class="h11"><a href="tel:0800757554">0-800-757-554</a></div>
                    <div class="h12"><a href="mailto:info@mokopizza.com">info@mokopizza.com</a></div>
                </div>
                <div class="social social_footer">
                    <div class="flex">
                        <div><a href="https://www.facebook.com/mokopizza/"><img src="{{ asset('img/fb.png') }}"></a></div>
                        <div><a href="https://www.instagram.com/mokopizza/"><img src="{{ asset('img/in.png') }}"></a></div>
                        <div><a href="https://vk.com/mokopizza"><img src="{{ asset('img/vk.png') }}"></a></div>
                        <div><a href="https://www.youtube.com/channel/UCOnXsiD--sleu3Qg_4xPkOA"><img src="{{ asset('img/yt.png') }}"></a></div>
                        <div><a href="https://t.me/mokopizza"><img src="{{ asset('img/t.png') }}"</a></div>
                    </div>
                    <div class="flex mob">
                        <div><a href="https://www.facebook.com/mokopizza/"><img src="{{ asset('img/fb1.png') }}"></a></div>
                        <div><a href="https://www.instagram.com/mokopizza/"><img src="{{ asset('img/in1.png') }}"></a></div>
                        <div><a href="https://vk.com/mokopizza"><img src="{{ asset('img/vk1.png') }}"></a></div>
                        <div><a href="https://t.me/mokopizza"><img src="{{ asset('img/t1.png') }}"></a></div>
                        <div><a href="https://www.instagram.com/mokopizza/"><img src="{{ asset('img/in1.png') }}"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="left-sidebar_w">
    <div id="left-sidebar" class="transition">
        <div id="mobile-navigation">
            <div class="top_nav">
                <ul>
                    <li><a href="#">Акции</a></li>
                    <li><a href="#">Вакансии</a></li>
                    <li><a href="#">Камера</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </div>
            <div class="h_w">
                <div class="h1"><p>Проспект Мира 87б</p></div>
                <div class="h2"><p>Работаем с 9:00 до 23:00</p></div>
                <div class="h2"><p>Бесплатная доставка</p></div>
            </div>
            <div class="social">
                <div class="flex">
                    <div><a href="#"><img src="{{ asset('img/fb1.png') }}" alt=""></a></div>
                    <div><a href="https://vk.com/mokopizza"><img src="{{ asset('img/vk1.png') }}" alt=""></a></div>
                    <div><a href="#"><img src="{{ asset('img/yt1.png') }}" alt=""></a></div>
                    <div><a href="https://t.me/mokopizza"><img src="{{ asset('img/t1.png') }}"></a></div>
                    <div><a href="https://www.instagram.com/mokopizza/"><img src="{{ asset('img/in1.png') }}"></a></div>

                </div>
            </div>
        </div>
    </div>
</div>
<header>
    <div class="header_mob">
        <div class="flex flex_sb flex_m">
            <div class="flex flex_m">
                <div class="tell">
                    <div><a href="tel:0800757554">0-800-757-554</a><p>Бесплатно с мобильных</p></div>
                </div>
                <div id="lines">
                    <div id="toggler" class="no-select">
                        <span class="transition left"></span>
                        <span class="transition right"></span>
                        <span class="hide"> </span>
                    </div>
                </div>
                <div class="logo"><a href="{{ route('site.index') }}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
                    <p class="logo_p">пр-т Мира 87б <a href="tel:0800757554">0-800-757-554</a></p>
                </div>
            </div>
            <div class="basket_b_w">
                <div class="basket_b">
                    <div><a href="{{ route('cart.index') }}" class="cart-sum"><span>{{ count_of_cart() }}</span></a></div>
                </div>
            </div>
        </div>
        <div class="nav">
                <nav>
                    <ul class="flex">
                        <li class="">
                            <a href="@if(URL::current() != route('site.index')){{ route('site.index') }}@endif#pizzas">Пиццы</a></li>
                            <li><a href="@if(URL::current() != route('site.index')){{ route('site.index') }}@endif#snacks">Закуски</a></li>
                            <li><a href="@if(URL::current() != route('site.index')){{ route('site.index') }}@endif#drinks">Напитки</a></li>
                            <li><a href="@if(URL::current() != route('site.index')){{ route('site.index') }}@endif#deserts">Десерты</a></li>
                        </ul>
                    </nav>
                </div>
    </div>
    <div class="header_w">
        <div class="top_header_w">
            <div class="inside">
                <div class="top_header">
                    <div class="flex">
                        <div class="h_w flex">
                            <div class="h1"><p>Проспект Мира 87б</p></div>
                            <div class="h2"><p>Работаем с 9:00 до 23:00</p></div>
                            <div class="h2"><p>Бесплатная доставка</p></div>
                        </div>
                        <div class="social">
                            <div class="flex">
                                <div><a href="https://www.facebook.com/mokopizza/"><img src="{{ asset('img/fb.png') }}"></a></div>
                                <div><a href="https://www.instagram.com/mokopizza/"><img src="{{ asset('img/in.png') }}"></a></div>
                                <div><a href="https://vk.com/mokopizza"><img src="{{ asset('img/vk.png') }}"></a></div>
                                <div><a href="https://www.youtube.com/channel/UCOnXsiD--sleu3Qg_4xPkOA"><img src="{{ asset('img/yt.png') }}"></a></div>
                                <div><a href="https://t.me/mokopizza"><img src="{{ asset('img/t.png') }}"></a></div>
                            </div>
                        </div>
                        <div class="top_nav">
                            <ul class="flex flex_sb">
                                <li><a href="#">Акции</a></li>
                                <li><a href="#">Вакансии</a></li>
                                <li><a href="#">Камера</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header">
            <div class="inside">
                <div class="flex flex_m">
                    <div class="logo"><a href="{{ route('site.index') }}"><img src="{{ asset('img/logo.png') }}" alt=""></a></div>
                    <div class="nav">
                        <nav>
                            <ul class="flex">
                                <li class="">
                                    <a href="@if(URL::current() != route('site.index')){{ route('site.index') }}@endif#pizzas">Пиццы</a></li>
                                <li><a href="@if(URL::current() != route('site.index')){{ route('site.index') }}@endif#snacks">Закуски</a></li>
                                <li><a href="@if(URL::current() != route('site.index')){{ route('site.index') }}@endif#drinks">Напитки</a></li>
                                <li><a href="@if(URL::current() != route('site.index')){{ route('site.index') }}@endif#deserts">Десерты</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="tell">
                        <div><a href="tel:0800757554">0-800-757-554</a><p>Бесплатно с мобильных</p></div>
                    </div>
                    <div class="basket_b active">
                        <div><a href="{{ route('cart.index') }}" class="cart-sum">{{ $sum }} грн<span>{{ count_of_cart() }}</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
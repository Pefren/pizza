<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать, как минимум, 6 символов и совпадать с подтверждением пароля.',
    'reset' => 'Ваш пароль успешно сброшен!',
    'sent' => 'Ссылка для сброса пароля отправлена на электронную почту!',
    'token' => 'Код для сброса пароля неверный.',
    'user' => "Пользователь с данным паролем не найден.",

];

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('phone');
	        $table->string('name');
	        $table->string('street');
	        $table->string('house');
	        $table->string('apartment');
	        $table->string('entrance');
	        $table->string('floor');
	        $table->string('change');
	        $table->text('comment');
	        $table->boolean('call_me');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Auth::routes();

// Home
Route::get( '/',                        [ 'as'=>'site.index', 'uses'=>'SiteController@index' ]);

// CSV
Route::group(['prefix' => 'csv'], function () {
	Route::get( '/',                    [ 'as' => 'csv.index', 'uses' => 'CsvController@index' ] );
	Route::post( '/import',             [ 'as' => 'csv.import', 'uses' => 'CsvController@import' ] );
});

// Cart
Route::group(['prefix' => 'cart'], function () {
	Route::get( '/',                    [ 'as' => 'cart.index', 'uses' => 'CartController@index' ] );
	Route::post( '/add',                [ 'as' => 'cart.add', 'uses' => 'CartController@add' ] );
	Route::post( '/remove',             [ 'as' => 'cart.remove', 'uses' => 'CartController@remove' ] );
	Route::post( '/change_quantity',    [ 'as' => 'cart.change_quantity', 'uses' => 'CartController@change_quantity' ] );
	Route::get( '/clear', function ()
	{
		Session::forget('cart');
	});
});

// Product
Route::group(['prefix' => 'product'], function () {
	Route::post( '/filter',    [ 'as' => 'product.filter', 'uses' => 'ProductController@filter' ] );
	Route::post( '/change_sauce',    [ 'as' => 'product.change_sauce', 'uses' => 'ProductController@change_sauce' ] );
});

// Order
Route::group(['prefix' => 'order'], function () {
	Route::post( '/',    [ 'as' => 'order.checkout', 'uses' => 'OrderController@checkout' ] );
});